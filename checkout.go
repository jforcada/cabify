package main

type order map[string]int

/*
Checkout object to process cabify shop orders
*/
type Checkout struct {
	order        order
	catalog      Catalog
	pricingRules map[string]*pricingRule
	scanner      chan string
	done         chan bool
}

/*
NewCheckout creates and initializes a Checkout object
*/
func NewCheckout(pricingRules map[string]*pricingRule) Checkout {
	co := Checkout{
		order:        make(order),
		catalog:      buildCatalog(),
		pricingRules: pricingRules,
		scanner:      make(chan string, 10),
		done:         make(chan bool)}

	go co.readScanner()

	return co
}

/*
Scan adds a new Product to the Checkout order
*/
func (co *Checkout) Scan(code string) {
	co.scanner <- code
}

func (co *Checkout) readScanner() {
	for code := range co.scanner {
		co.addToOrder(code)
	}
	co.done <- true
}

func (co *Checkout) addToOrder(code string) {
	product := GetProduct(co.catalog, code)
	if product != nil {
		co.order[code]++
	}
}

/*
GetTotal calculates the final order price
*/
func (co *Checkout) GetTotal() int64 {
	var total int64
	var product *Product

	// Close the channel to start the order calculations
	close(co.scanner)
	<-co.done

	for prodCode, quantity := range co.order {
		if rule, ok := co.pricingRules[prodCode]; ok {
			rule.discriminator += quantity
		}

		product = GetProduct(co.catalog, prodCode)
		total += product.price * int64(quantity)
	}

	for code, rule := range co.pricingRules {
		total = rule.apply(total, co.catalog, code)
	}

	return total
}
