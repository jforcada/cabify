package main

import "testing"

func TestApplyRuleTypeDiscountXForEqualOrGreaterY(t *testing.T) {
	catalog := buildCatalog()
	rule := pricingRule{
		ruleType:       RuleTypeDiscountXForEqualOrGreaterY,
		discrimination: 4,
		discriminator:  4,
		discountFactor: 50}

	newPrice := rule.apply(3000, catalog, "VOUCHER")
	if newPrice != 2000 {
		t.Errorf("RuleTypeDiscountXForEqualOrGreaterY: Discount bad calculated: %d", newPrice)
	}
}

func TestApplyRuleTypeOneFreeEachX(t *testing.T) {
	catalog := buildCatalog()
	rule := pricingRule{
		ruleType:       RuleTypeOneFreeEachX,
		discrimination: 5,
		discriminator:  5}

	newPrice := rule.apply(10000, catalog, "TSHIRT")
	if newPrice != 8000 {
		t.Errorf("RuleTypeOneFreeEachX: Discount bad calculated: %d", newPrice)
	}
}

func TestApplyWithNonExistingRule(t *testing.T) {
	catalog := buildCatalog()
	var myNewImaginaryRuleType ruleType = 20
	rule := pricingRule{
		ruleType:       myNewImaginaryRuleType,
		discrimination: 5,
		discriminator:  5}

	newPrice := rule.apply(10000, catalog, "TSHIRT")
	if newPrice != 10000 {
		t.Errorf("Apply should make no discount for invented rules, got: %d", newPrice)
	}
}

func TestApplyToNonExistingProduct(t *testing.T) {
	catalog := buildCatalog()
	rule := pricingRule{
		ruleType:       RuleTypeOneFreeEachX,
		discrimination: 5,
		discriminator:  5}

	newPrice := rule.apply(10000, catalog, "TROLOLO")
	if newPrice != 10000 {
		t.Errorf("Apply should make no discount for unknown products, got: %d", newPrice)
	}
}
