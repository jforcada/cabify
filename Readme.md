Cabify code test
================

To run demo:

    $ go run cabify.go checkout.go rules.go catalog.go

To run tests:

    $ go test

Possible improvements
---------------------
- Add tests to stress synchronization mechanism
- Create some interfaces to build more decoupled parts:
  + for the `Product` type
  + for the `Catalog` type
  + for the `PricingRule` type
