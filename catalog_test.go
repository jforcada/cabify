package main

import "testing"

func TestCreateProductReturnsProductOk(t *testing.T) {
	hoodie := createProduct("HOODIE", "Cabify Hoodie", 3500)
	if hoodie.code != "HOODIE" {
		t.Errorf("createProduct does not assign correct code: %s", hoodie.code)
	}
	if hoodie.name != "Cabify Hoodie" {
		t.Errorf("createProduct does not assign correct name: %s", hoodie.name)
	}
	if hoodie.price != 3500 {
		t.Errorf("createProduct does not assign correct price: %d", hoodie.price)
	}
}

func TestGetProductReturnsProductRefOkIfExists(t *testing.T) {
	var product *Product
	catalog := buildCatalog()
	product = GetProduct(catalog, "TSHIRT")
	if product.code != "TSHIRT" {
		t.Errorf("GetProduct did return a wrong product: %s", product.code)
	}
}

func TestGetProductReturnsNilIfNotExists(t *testing.T) {
	var product *Product
	catalog := buildCatalog()
	product = GetProduct(catalog, "TROLOLO")
	if product != nil {
		t.Errorf("GetProduct did return a nonexisten product: %s", product)
	}
}

func TestBuildCatalogReturnsCatalog(t *testing.T) {
	catalog := buildCatalog()
	if len(catalog) == 0 {
		t.Errorf("buildCatalog failed to return a non empty Catalog")
	} else if len(catalog) != 3 {
		t.Errorf("buildCatalog failed to return the correct product elements: %d", len(catalog))
	}
}
