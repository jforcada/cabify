package main

/*
ruleType defines the type of a PricingRule
*/
type ruleType int

const (
	// RuleTypeOneFreeEachX discounts one full item each X (discrimination) items of the same time
	RuleTypeOneFreeEachX ruleType = iota
	// RuleTypeDiscountXForEqualOrGreaterY discounts a X (discountFactor) percent of the price
	// of all items when they are greater than Y (discrimination)
	RuleTypeDiscountXForEqualOrGreaterY
)

/*
pricingRule configures the discounts of a checkout
*/
type pricingRule struct {
	ruleType       ruleType
	discrimination int
	discriminator  int
	discountFactor int
}

/*
apply the given PricingRule
*/
func (pr pricingRule) apply(onQuantity int64, catalog Catalog, code string) int64 {
	product := GetProduct(catalog, code)
	if product != nil {
		switch pr.ruleType {
		case RuleTypeDiscountXForEqualOrGreaterY:
			return pr.runRuleTypeDiscountXForEqualOrGreaterY(onQuantity, product.price)
		case RuleTypeOneFreeEachX:
			return pr.runRuleTypeOneFreeEachX(onQuantity, product.price)
		}
	}
	return onQuantity
}

func (pr pricingRule) runRuleTypeDiscountXForEqualOrGreaterY(onQuantity int64, unitPrice int64) int64 {
	var discountValue int64
	if pr.discriminator >= pr.discrimination {
		discountPerUnit := unitPrice * int64(pr.discountFactor) / 100
		discountValue = discountPerUnit * int64(pr.discriminator)
	}
	return onQuantity - discountValue
}

func (pr pricingRule) runRuleTypeOneFreeEachX(onQuantity int64, unitPrice int64) int64 {
	groupsOfItems := pr.discriminator / pr.discrimination
	discountValue := int64(groupsOfItems) * unitPrice
	return onQuantity - discountValue
}
