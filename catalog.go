package main

/*
Product represents an item sold by the cabify store
*/
type Product struct {
	code  string
	name  string
	price int64 // in cents
}

/*
Catalog represents a set of products available
*/
type Catalog map[string]Product

func createProduct(code string, name string, priceInCents int64) Product {
	return Product{code, name, priceInCents}
}

/*
GetProduct returns the reference of Product of a catalog given its code
*/
func GetProduct(catalog Catalog, code string) *Product {
	if product, ok := catalog[code]; ok {
		return &(product)
	}
	return nil
}

func buildCatalog() Catalog {
	voucher := createProduct("VOUCHER", "Cabify Voucher", 500)
	tshirt := createProduct("TSHIRT", "Cabify T-Shirt", 2000)
	mug := createProduct("MUG", "Cabify Coffee Mug", 750)
	catalog := Catalog{"VOUCHER": voucher, "TSHIRT": tshirt, "MUG": mug}
	return catalog
}
