package main

import (
	"testing"
)

func TestScanProductCode(t *testing.T) {
	checkout := NewCheckout(make(map[string]*pricingRule))
	for prodCode, quantity := range checkout.order {
		if quantity != 0 {
			t.Errorf("Scan: checkout starts with nonempty order of %s:%d", prodCode, quantity)
		}
	}

	checkout.Scan("TSHIRT")
	checkout.Scan("VOUCHER")
	checkout.Scan("MUG")

	for prodCode, quantity := range checkout.order {
		if quantity != 1 {
			t.Errorf("Scan: improperly working for %s:%d", prodCode, quantity)
		}
	}
}

func TestScanUnknownProductCode(t *testing.T) {
	checkout := NewCheckout(make(map[string]*pricingRule))
	checkout.Scan("TROLOLO")
	for prodCode, quantity := range checkout.order {
		if quantity != 0 {
			t.Errorf("Scan: is adding unknown product: %s to: %s", "TROLOLO", prodCode)
		}
	}
}

func TestGetTotalWithoutPricingRules(t *testing.T) {
	checkout := NewCheckout(make(map[string]*pricingRule))
	checkout.Scan("TSHIRT")
	checkout.Scan("TSHIRT")
	checkout.Scan("VOUCHER")
	checkout.Scan("VOUCHER")
	checkout.Scan("VOUCHER")
	checkout.Scan("MUG")

	orderPrice := checkout.GetTotal()
	if orderPrice != 6250 {
		t.Errorf("GetTotal: calculates wrong without pricing rules: %d", orderPrice)
	}
}

func TestGetTotalWithSomePricingRules(t *testing.T) {
	rule1 := pricingRule{
		ruleType:       RuleTypeDiscountXForEqualOrGreaterY,
		discrimination: 3,
		discountFactor: 5}
	rule2 := pricingRule{
		ruleType:       RuleTypeOneFreeEachX,
		discrimination: 3}
	pricingRules := map[string]*pricingRule{
		"TSHIRT":  &rule1,
		"VOUCHER": &rule2}
	checkout := NewCheckout(pricingRules)
	checkout.Scan("TSHIRT")
	checkout.Scan("TSHIRT")
	checkout.Scan("TSHIRT")
	checkout.Scan("VOUCHER")
	checkout.Scan("VOUCHER")
	checkout.Scan("VOUCHER")
	checkout.Scan("MUG")

	orderPrice := checkout.GetTotal()
	if orderPrice != 7450 {
		t.Errorf("GetTotal: calculates wrong with all pricing rules: %d", orderPrice)
	}
}
