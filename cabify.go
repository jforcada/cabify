package main

import "fmt"

func main() {
	done := make(chan bool)
	fmt.Println("Cabify Store")

	// Create rules
	rule1 := pricingRule{ruleType: RuleTypeDiscountXForEqualOrGreaterY, discrimination: 3, discountFactor: 5}
	rule2 := pricingRule{ruleType: RuleTypeOneFreeEachX, discrimination: 3}
	pricingRules := map[string]*pricingRule{
		"TSHIRT":  &rule1,
		"VOUCHER": &rule2}

	// Create new checkout
	co := NewCheckout(pricingRules)

	// Build order
	go func() {
		co.Scan("VOUCHER")
		co.Scan("VOUCHER")
		co.Scan("VOUCHER")
		co.Scan("TSHIRT")
		co.Scan("MUG")
		done <- true
	}()
	go func() {
		co.Scan("TSHIRT")
		co.Scan("TSHIRT")
		co.Scan("TSHIRT")
		co.Scan("VOUCHER")
		co.Scan("MUG")
		done <- true
	}()

	<-done
	<-done

	// Calculate price order
	total := co.GetTotal()
	fmt.Println("Order Total:", float64(total)/100)
}
